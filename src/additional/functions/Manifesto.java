package additional.functions;

import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Interfaccia manifesti 
 * @author Helena Zaccarelli
 */

public interface Manifesto {

    /*enum Stato {
        ARRIVO, PARTENZA;
    }*/
    
    enum Dato {
        COLLI, PESI;
    }

    /**
     * Metodo che ritorna i porti di arrivo e destinazione della nave
     * @param voyageID
     * @return
     */
    List<String> itinerary(int voyageID);

    /**
     * Metodo che ritorna i dati del manifesto di arrivo
     * @param voyageID
     * @return
     */
    Vector<Object> createArrivalManifest(int voyageID);

    /**
     * Metodo che ritorna i colli e pesi totali previsti in imbarco
     * @param voyageID
     * @param type
     * @return
     */
    int manifestSum(int voyageID, Dato type);
    
    /**
     * Metodo che ritorna i colli o il peso di un dato singolo
     * @param seslectedRow
     * @return
     */
    List<Integer> selectedRowQuantity(int selectedRow);

    /**
     * Metodo che ritorna la data del viaggio
     * @param voyageID
     * @return
     */
    Date voyageDate(int voyageID);
}
