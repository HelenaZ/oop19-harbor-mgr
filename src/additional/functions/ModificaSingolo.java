package additional.functions;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import controller.implementation.*;
import exception.ExceptionNegativeQty;

/**
 * Frame intemedio che permette la modifica di colli e peso di un singolo selezionato dal manifesto di partenza
 * @author Helena Zaccarelli
 */
public class ModificaSingolo extends JFrame {

    private static final long serialVersionUID = 4132182711366062593L;
    private int newWeight;
    private int newItemsNumber;
    
    /**
     *  Creazione del frame
     */    
    public ModificaSingolo(int items, int weight, int selectedRow, int selectedVoyage) {
        
        setTitle("MODIFICA COLLI E PESI SINGOLO");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(550, 250, 363, 322);
        
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);
        
        JLabel lblOriginalItemsNumber = new JLabel("COLLI ORIGINALI:");
        lblOriginalItemsNumber.setBounds(23, 23, 134, 22);
        contentPane.add(lblOriginalItemsNumber);
        
        JLabel originalItemsNumber = new JLabel(String.valueOf(items));
        originalItemsNumber.setBounds(167, 23, 126, 22);
        contentPane.add(originalItemsNumber);
        
        JLabel lblOriginalWeight = new JLabel("PESO ORIGINALE (TN):");
        lblOriginalWeight.setBounds(23, 55, 134, 22);
        contentPane.add(lblOriginalWeight);
        
        JLabel originalWeight = new JLabel(String.valueOf(weight));
        originalWeight.setBounds(167, 55, 126, 22);
        contentPane.add(originalWeight);
                
        JLabel lblNewItemsNumnber = new JLabel("COLLI CORRETTI:");
        lblNewItemsNumnber.setBounds(23, 88, 134, 22);
        contentPane.add(lblNewItemsNumnber);
        
        JTextField jtfNewItemsNumber = new JTextField("");
        jtfNewItemsNumber.setToolTipText("Inserisci il numero di colli corretti");
        jtfNewItemsNumber.setBounds(167, 88, 126, 22);
        contentPane.add(jtfNewItemsNumber);
        
        JLabel lblNewWeight = new JLabel("PESO CORRETTO (TN):");
        lblNewWeight.setBounds(23, 121, 134, 22);
        contentPane.add(lblNewWeight);
        
        JTextField jtfNewWeight = new JTextField("");
        jtfNewWeight.setToolTipText("Inserisci il peso corretto");
        jtfNewWeight.setBounds(167, 121, 126, 22);
        contentPane.add(jtfNewWeight);
        
        JButton btnConfirm = new JButton("CONFERMA");
        btnConfirm.setBounds(75, 168, 190, 38);
        contentPane.add(btnConfirm);
        
        JButton btnBack = new JButton("INDIETRO");
        btnBack.setBounds(23, 237, 108, 28);
        contentPane.add(btnBack);
        
        /**
         *  Implementazione dell'ActionListener relativo al tasto "Conferma"
         */
        btnConfirm.addActionListener((a) -> {
            try {
                newWeight = Integer.parseInt(jtfNewWeight.getText());
            }catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "Prego inserire un numero intero.");
                return;
            }
            try {
                newItemsNumber = Integer.parseInt(jtfNewItemsNumber.getText());
            }catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "Prego inserire un numero intero.");
                return;
            }
            if((newWeight > 0) && (newItemsNumber > 0)) {
                try {
                    /*
                     * Il metodo aggiornaColliPesi è un metodo aggiuntivo/nuovo inserito in GestionePrenotazioni
                     */
                    GestionePrenotazione.creaIst().aggiornaPrenotazione(newItemsNumber, newWeight, selectedRow, selectedVoyage);
                    JOptionPane.showMessageDialog(null, "Aggiornamento dati avvenuto correttamente. \n Ricordati di aggiornare il manifesto con l'apposito tasto!");
                    close();
                }catch (ExceptionNegativeQty e) {
                    JOptionPane.showMessageDialog(null, "Operazione non possibile, portata nave superata.");
                }
            }else {
                JOptionPane.showMessageDialog(null, "Prego inserire valori positivi");
            }
        });
        
        btnBack.addActionListener ((a) -> {
            this.close();
        });
    }
    
    /**
     *  Chiusura del frame
     */
    public void close() {
        setVisible(false);
        dispose();
    }   
}
