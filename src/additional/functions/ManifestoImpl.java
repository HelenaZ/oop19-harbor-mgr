package additional.functions;

import java.text.SimpleDateFormat;
import java.util.*;
import controller.implementation.*;
import model.Implementations.*;

/**
 * Implementazione di un manifesto
 * @author Helena Zaccarelli
 *
 */

public class ManifestoImpl implements Manifesto {
    
    private final GestioneViaggio gestioneViaggio = GestioneViaggio.creaIst();
    private final GestionePrenotazione gestionePrenotazioni = GestionePrenotazione.creaIst();
    
    @Override
    public List<String> itinerary(int voyageID) {
        List<String> itinerary = new ArrayList<>();
        gestioneViaggio.getList().stream()              
                .filter(v -> v.getId() == voyageID)
                .forEach(v -> {
                    itinerary.add(v.getProvenienza());
                    itinerary.add(v.getDestinazione());
                });
        return itinerary;
        
      /*String port = null;
        for(ImplViaggio voyage : gestioneViaggio.getList()) {
                if (voyage.getId() == voyageID) {
                    if (vesselState.equals(Stato.ARRIVO)) {
                        port = voyage.getProvenienza(); 
                    } else {
                        port = voyage.getDestinazione();
                    }
                }
        }
        return port;*/
    }

    @Override
    public Vector<Object> createArrivalManifest(int v) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Vector<Object> report = new Vector<Object>();
        for (ImplViaggio voyage : gestioneViaggio.getList()) {
            if (voyage.getId() == v) {
                report = new Vector<>(List.of(voyage.getId(),dateFormat.format(voyage.getData()), voyage.getNome(),voyage.getBandiera(),voyage.getTipo(), voyage.getSbarco()));
            }
        }
        return report;
    }

    @Override
    public int manifestSum(int voyageID, Dato type) {
        int sum = 0;
        for(ImplPrenotazione pren : gestionePrenotazioni.getList()) {
            if (pren.getId() == voyageID) {
                if (type.equals(Dato.COLLI)) {
                    sum = sum + pren.getColli(); 
                } else {
                    sum = sum + pren.getPeso();
                }
            }
        }
        return sum;
    }
    
    public List<Integer> selectedRowQuantity(int selectedRow) {
        List<Integer> values = new ArrayList<>();
        gestionePrenotazioni.getList().stream()              
                .filter(p -> p.getCodice() == selectedRow)
                .forEach(p -> {
                    values.add(p.getColli());
                    values.add(p.getPeso());
                });
        return values;
        
      /*int value = 0;
        for(ImplPrenotazione p : gestionePrenotazioni.getList()) {
            if (p.getCodice() == selectedRow) {
                if (type.equals(Dato.COLLI)) {
                    value = p.getColli(); 
                } else {
                    value = p.getPeso();
                }
            }
        }
        return value;*/
    }

    @Override
    public Date voyageDate(int vojageID) {
        Date voyageDate = new Date(0);
        for (ImplViaggio v : gestioneViaggio.getList()) {
            if (v.getId() == vojageID) {
                voyageDate = v.getData();
            }
        }
        return voyageDate;
    }
}

