package additional.functions;

import java.util.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import additional.functions.Manifesto.*;
import controller.implementation.*;
import model.Implementations.*;
import view.Main.Main;

/**
 * Frame di generazione e visualizzazione dei manifesti 
 * @author Helena Zaccarelli
 */

public class VisualizzaManifesto extends JFrame{

    private static final long serialVersionUID = 5119453879843337845L;
    private final JComboBox<String> cmbVessel = new JComboBox<String>();
    private final JComboBox<Integer> cmbVoyage = new JComboBox<Integer>();
    private final JTable arrivalManifest = new JTable();
    private final JTable departureManifest = new JTable();
    private final JLabel departurManifestItems = new JLabel();
    private final JLabel departureManifestWeight = new JLabel();
    private final GestioneNave gestioneNave = GestioneNave.creaIst();
    private final GestioneViaggio gestioneViaggio = GestioneViaggio.creaIst();
    private final GestionePrenotazione gestionePrenotazioni = GestionePrenotazione.creaIst();
    private String selectedVessel;
    private int selectedVoyage;
    private Date voyageDate;
    private final Manifesto manifests;
    
    DefaultTableModel dtm1 = new DefaultTableModel() {
        private static final long serialVersionUID = 1L;

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };
    
    /**
     *  Creazione del frame
     */     
    public VisualizzaManifesto() {
        this.manifests = new ManifestoImpl();
        setTitle("MANIFESTI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 700, 610);
        
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);
        
        JLabel lblVessel = new JLabel("Nome nave:");
        lblVessel.setBounds(23, 15, 165, 14);
        contentPane.add(lblVessel);
        
        cmbVessel.setBounds(111, 11, 126, 22);
        contentPane.add(cmbVessel);
        editComboNave();
        
        JButton btnConfirmVessel = new JButton("Conferma nave");
        btnConfirmVessel.setBounds(257, 11, 196, 22);
        contentPane.add(btnConfirmVessel);
        
        JLabel lblVoyage = new JLabel("Codice viaggio:");
        lblVoyage.setBounds(23, 57, 165, 14);
        contentPane.add(lblVoyage);
        
        cmbVoyage.setBounds(111, 53, 126, 22);
        contentPane.add(cmbVoyage);
        cmbVoyage.setEnabled(false);
        
        JButton btnConfirmVoyage = new JButton("Conferma viaggio");
        btnConfirmVoyage.setBounds(257, 53, 196, 22);
        contentPane.add(btnConfirmVoyage);
        btnConfirmVoyage.setEnabled(false);

        JButton btnCreateManifest = new JButton("CREA MANIFESTI");
        btnCreateManifest.setBounds(483, 15, 145, 38);
        contentPane.add(btnCreateManifest);
        btnCreateManifest.setEnabled(false);
        
        JScrollPane scrollPaneMMA = new JScrollPane();
        scrollPaneMMA.setBounds(20, 140, 632, 55);
        contentPane.add(scrollPaneMMA);
        scrollPaneMMA.setViewportView(arrivalManifest);
        
        JScrollPane scrollPaneMMP = new JScrollPane();
        scrollPaneMMP.setBounds(20, 232, 632, 234);
        contentPane.add(scrollPaneMMP);
        scrollPaneMMP.setViewportView(departureManifest);
        departureManifest.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JLabel lblMMA = new JLabel("MANIFESTO DI ARRIVO");
        lblMMA.setBounds(20, 113, 145, 28);
        contentPane.add(lblMMA);
        
        JLabel lblMMP = new JLabel("MANIFESTO DI PARTENZA");
        lblMMP.setBounds(20, 206, 168, 28);
        contentPane.add(lblMMP);
        
        JLabel lblPreviousPort = new JLabel("Porto di provenienza:");
        lblPreviousPort.setBounds(365, 113, 120, 28);
        contentPane.add(lblPreviousPort);
        
        JLabel lblNextPort = new JLabel("Porto di destinazione:");
        lblNextPort.setBounds(365, 206, 139, 28);
        contentPane.add(lblNextPort);
        
        JLabel provenance = new JLabel();
        provenance.setBounds(514, 113, 138, 28);
        contentPane.add(provenance);
        
        JLabel destination = new JLabel();
        destination.setBounds(514, 206, 138, 28);
        contentPane.add(destination);
        
        JLabel lblMMPItems = new JLabel("Totale colli in imbarco:");
        lblMMPItems.setBounds(250, 468, 133, 28);
        contentPane.add(lblMMPItems);
        
        JLabel lblMMPWeight = new JLabel("Totale pesi (Tn):");
        lblMMPWeight.setBounds(460, 468, 96, 28);
        contentPane.add(lblMMPWeight);
        
        departurManifestItems.setBounds(393, 468, 60, 28);
        contentPane.add(departurManifestItems);

        departureManifestWeight.setBounds(556, 468, 96, 28);
        contentPane.add(departureManifestWeight);
        
        JButton btnChangeValues = new JButton("CORREGGI SINGOLO MMP");
        btnChangeValues.setBounds(23, 471, 190, 25);
        contentPane.add(btnChangeValues);
        btnChangeValues.setEnabled(false);
        
        JButton btnUpdate = new JButton("AGGIORNA MMP");
        btnUpdate.setBounds(23, 494, 190, 25);
        contentPane.add(btnUpdate);
        btnUpdate.setEnabled(false);
        
        JButton btnBack = new JButton("INDIETRO");
        btnBack.setBounds(23, 526, 108, 28);
        contentPane.add(btnBack);
        
        JButton btnSendVesselDeparture = new JButton("CONFERMA MANIFESTO E INVIA PARTENZA");
        btnSendVesselDeparture.setBounds(337, 521, 313, 38);
        contentPane.add(btnSendVesselDeparture);
        btnSendVesselDeparture.setEnabled(false);
        
        /**
         *  Implementazione degli ActionListener relativi ai testi presenti nel frame
         */
        btnConfirmVessel.addActionListener((a)-> {
                selectedVessel=cmbVessel.getSelectedItem().toString();
                cmbVessel.setEnabled(false);
                btnConfirmVessel.setEnabled(false);
                cmbVoyage.setEnabled(true);
                btnConfirmVoyage.setEnabled(true);
                editComboViaggio();
        });
        
        btnConfirmVoyage.addActionListener((a)-> {
            if (cmbVoyage.getItemCount() == 0) {
                JOptionPane.showInternalMessageDialog(null, "Nessun viaggio disponibile per questa nave");
                cmbVessel.setEnabled(true);
                btnConfirmVessel.setEnabled(true);
            } else {
                selectedVoyage= (int)cmbVoyage.getSelectedItem();
                voyageDate = manifests.voyageDate(selectedVoyage);
                btnCreateManifest.setEnabled(true);
            }
            cmbVoyage.setEnabled(false);
            btnConfirmVoyage.setEnabled(false);
        });
        
        btnCreateManifest.addActionListener((a)-> {
                                List<String> itinerary = manifests.itinerary(selectedVoyage);
                                provenance.setText(itinerary.get(0));
                                //provenance.setText(manifests.itinerary(selectedVoyage, Stato.ARRIVO));
                                this.updateArrivalManifest();
                                destination.setText(itinerary.get(1));
                                //destination.setText(manifests.itinerary(selectedVoyage, Stato.PARTENZA));
                                this.updateDepartureManifest();
                                btnCreateManifest.setEnabled(false);
                                //System.out.println(""+dtm1.getRowCount());
                                if (dtm1.getRowCount()>0) {
                                    btnChangeValues.setEnabled(true);
                                }
                                btnSendVesselDeparture.setEnabled(true);
                                this.updateTotals(selectedVoyage);
        });
        
        btnChangeValues.addActionListener((a) -> {
            DefaultTableModel model = (DefaultTableModel) departureManifest.getModel();
            if(departureManifest.getSelectedRow()>=0) {
                int selectedRow = (int) model.getValueAt(departureManifest.getSelectedRow(), 0);
                //System.out.println("Singolo selezionato: "+selectedRow);
                List<Integer> values = manifests.selectedRowQuantity(selectedRow);
                new ModificaSingolo(values.get(0), values.get(1), selectedRow, selectedVoyage).setVisible(true);
                //new ModificaSingolo(manifests.selectedRowQuantity(selectedRow, Dato.COLLI), manifests.selectedRowQuantity(selectedRow, Dato.PESI), selectedRow, selectedVoyage).setVisible(true);
                btnChangeValues.setEnabled(false);
                btnSendVesselDeparture.setEnabled(false);
                btnUpdate.setEnabled(true);
            } else {
                JOptionPane.showInternalMessageDialog(null, "Seleziona un singolo!");
                
            }
            
        });
        
        btnUpdate.addActionListener((a) -> {
            dtm1.setRowCount(0);
            this.updateDepartureManifest();
            this.updateTotals(selectedVoyage);
            btnChangeValues.setEnabled(true);
            btnSendVesselDeparture.setEnabled(true);
            btnUpdate.setEnabled(false);
        });
        
        btnBack.addActionListener ((a) -> {
            new Main().setVisible(true);
            setVisible(false);
        });
        
        btnSendVesselDeparture.addActionListener((a -> {
            if (voyageDate.before(new Date())) {
                int confirm = JOptionPane.showConfirmDialog(null, "Operazione irreversibile: siete sicuri di voler procedere con l'invio? \n Tutte le prenotazioni verranno eliminate dall'archvio.", "Sì", JOptionPane.YES_NO_OPTION);
                if (confirm == JOptionPane.YES_OPTION) { 
                    gestioneViaggio.partenzaViaggio(selectedVoyage);
                    JOptionPane.showInternalMessageDialog(null, "Invio effettuato");
                    btnBack.doClick();
                } else {
                    JOptionPane.showInternalMessageDialog(null, "Operazione annullata");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Impossibile impostare il viaggio come partito. Data arrivo successiva a quella odierna.");
                btnBack.doClick();
            }
        }));
    }

    /**
     *  Metodo che popola la ComboBox Nave
     */
    public void editComboNave() {
        for(ImplNave nave : gestioneNave.getList()) {
                cmbVessel.addItem(nave.getNome());
        }  
    }
    
    /**
     *  Metodo che popola la ComboBox Viaggio 
     */
    public void editComboViaggio() {
            //In commento comandi di stampa su console utili per la verifica del corretto funzionamento del metodo 
            //System.out.println("Viaggi presenti in archivio:");
            for(ImplViaggio viaggio : gestioneViaggio.getList()) {
                    //System.out.println("Nave "+voyageID.getNome() + ", Id viaggio "+voyageID.getId() +", già ripartita "+voyageID.getPartenza());
                    if (viaggio.getPartenza() == false && viaggio.getNome().equals(selectedVessel)) {
                        cmbVoyage.addItem(viaggio.getId());
                    }
            }
    }
    
    /**
     *  Metodo che genera il manifesto di arrivo
     */
    public void updateArrivalManifest() {
        DefaultTableModel dtm = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String arrivo[] = new String[] { "ID viaggio", "Data",  "Nome Nave", "Bandiera", "Tipo Merce", "Peso in Sbarco"};
        dtm.setColumnIdentifiers(arrivo);
        arrivalManifest.getTableHeader().setReorderingAllowed(false);
        arrivalManifest.setModel(dtm);
        dtm.addRow(manifests.createArrivalManifest(selectedVoyage));  
    }
    
    /**
     *  Metodo che genera/aggiorna il manifesto di arrivo
     */
    public void updateDepartureManifest() {
        String partenza[] = new String[] { "ID singolo", "Tipo Merce", "Colli", "Peso (Tn)"};
        dtm1.setColumnIdentifiers(partenza);
        departureManifest.getTableHeader().setReorderingAllowed(false);
        departureManifest.setModel(dtm1);
        for (ImplPrenotazione pren : gestionePrenotazioni.getList()) {
            if (pren.getId() == selectedVoyage) {
                dtm1.addRow(new Vector<>(List.of(pren.getCodice(),pren.getTipo(), pren.getColli(), pren.getPeso())));
            }
        }  
    }
    
    /**
     *  Metodo che genera/aggiorna l'indicazione dei colli e pesi totali previsti in imbarco
     */
    private void updateTotals(int viaggio) {
        departurManifestItems.setText(String.valueOf(manifests.manifestSum(selectedVoyage, Dato.COLLI)));
        departureManifestWeight.setText(String.valueOf(manifests.manifestSum(selectedVoyage, Dato.PESI)));
    }
}
